package org.digieng.repostats.model

import kotlinx.serialization.Serializable
import org.digieng.repostats.BuildSystem

@Serializable
data class Repo(
    val id: String,
    val name: String,
    val description: String,
    val webUrl: String,
    val totalOpenIssues: Long,
    val buildSystem: BuildSystem = BuildSystem.UNKNOWN,
    val languages: Map<String, Double> = mapOf()
) {
    override fun toString(): String = "${this::class.simpleName}(id=$id, name=$name, totalOpenIssues=" +
        "$totalOpenIssues, languages=$languages, buildSystem=$buildSystem)"
}
