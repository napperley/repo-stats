package org.digieng.repostats

import org.digieng.repostats.model.Repo

object GroupStatistics {
    fun averageOpenIssues(repos: List<Repo>): Long {
        var total = 0L
        repos.forEach { total += it.totalOpenIssues }
        return if (repos.isNotEmpty() && total > 0) total / repos.size else total
    }

    fun mainLanguage(repos: List<Repo>): String = repos
        .flatMap { it.languages.keys }
        .asSequence()
        .groupBy { it }
        .mapValues { it.value.size }
        .maxByOrNull { it.value }?.key ?: ""

    fun languagesUsed(repos: List<Repo>): Set<String> = repos
        .flatMap { it.languages.keys }
        .asSequence()
        .distinct()
        .toSet()

    fun mostUsedLanguage(repos: List<Repo>): String = repos
        .mapNotNull { r -> r.languages.maxByOrNull { l -> l.value } }
        .groupBy { it.key }
        .mapValues { group ->
            group.value.asSequence().map { item -> item.value }.reduce { acc, dblVal -> acc + dblVal }
        }
        .maxByOrNull { it.value }
        ?.key ?: ""

    fun mainBuildSystem(repos: List<Repo>): BuildSystem =
        repos.map { it.buildSystem }.maxByOrNull { it } ?: BuildSystem.UNKNOWN
}
