package org.digieng.repostats

enum class BuildSystem(val fileName: String, val systemName: String) {
    MAVEN("pom.xml", "Maven"),
    GRADLE("build.gradle", "Gradle (Groovy DSL)"),
    GRADLE_KOTLIN("build.gradle.kts", "Gradle (Kotlin DSL)"),
    UNKNOWN("", "")
}
