package org.digieng.repostats.query

import org.digieng.repostats.model.Repo

interface GroupQuery {
    suspend fun allRepositoriesById(id: String): List<Repo>
    suspend fun allRepositoriesByName(name: String): List<Repo>
}
