package org.digieng.repostats

import kotlinx.coroutines.runBlocking
import org.digieng.repostats.query.GitLabGroupQuery
import kotlin.system.exitProcess
import io.ktor.client.*
import io.ktor.client.engine.apache.*
import kotlinx.coroutines.coroutineScope

internal const val HTTP_TIMEOUT = 5000
internal val httpClient = HttpClient(Apache)
private const val GIT_LAB = "git_lab"

fun main(args: Array<String>) = runBlocking {
    checkArgsSize(args)
    val group = fetchGroupArg(args)
    if (fetchProviderArg(args) == GIT_LAB) {
        println("Fetching repo stats for $group...")
        listGitLabRepositories(group)
    }
    println("Exiting...")
    Unit
}

private fun checkArgsSize(args: Array<String>) {
    val usageMsg = """
        Usage:
          java -jar repo_stats.jar code_hosting_provider group

        * code_hosting_provider - Name of the provider that hosts the code. Must be one of the following:
            git_lab
    """.trimIndent()
    val requiredArgs = 2
    if (args.size != requiredArgs) {
        println("$usageMsg\n\nExiting...")
        exitProcess(-1)
    }
}

private fun fetchProviderArg(args: Array<String>): String {
    val supportedProviders = setOf(GIT_LAB)
    val msg = """
        Unsupported code hosting provider for the second argument, must be one of the following:
          * git_lab
    """.trimIndent()
    val providerPos = 0
    val provider = args[providerPos]
    if (provider !in supportedProviders) {
        println(msg)
        exitProcess(-1)
    }
    return provider
}

private fun fetchGroupArg(args: Array<String>): String {
    val groupPos = 1
    val group = args[groupPos].trim().replace("\"", "")
    if (group.isEmpty()) {
        println("A group must be specified")
        exitProcess(-1)
    }
    return group
}

private suspend fun listGitLabRepositories(group: String) = coroutineScope {
    try {
        val repos = GitLabGroupQuery.allRepositoriesByName(group)
        val mainBuildSystem = GroupStatistics.mainBuildSystem(repos)
        repos.forEach { println(it) }
        println("Main Language: ${GroupStatistics.mainLanguage(repos)}")
        println("Average Open Issues: ${GroupStatistics.averageOpenIssues(repos)}")
        println("Languages Used: ${GroupStatistics.languagesUsed(repos)}")
        println("Most Used Language: ${GroupStatistics.mostUsedLanguage(repos)}")
        println("Main Build System: " +
            if (mainBuildSystem != BuildSystem.UNKNOWN) mainBuildSystem.systemName else "Unknown")
    } catch (ex: Exception) {
        println("Cannot list repositories: ${ex.message}")
    }
}
