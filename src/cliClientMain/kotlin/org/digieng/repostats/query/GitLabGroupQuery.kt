package org.digieng.repostats.query

import io.ktor.client.request.*
import kotlinx.coroutines.withTimeout
import org.digieng.repostats.HTTP_TIMEOUT
import org.digieng.repostats.httpClient
import org.digieng.repostats.model.GitLabRepo
import org.digieng.repostats.model.Repo

actual object GitLabGroupQuery : GroupQuery {
    private const val API_URL = "http://localhost:8080"
    private const val GIT_LAB = "git_lab"

    override suspend fun allRepositoriesById(id: String): List<Repo> {
        TODO("not implemented")
    }

    override suspend fun allRepositoriesByName(name: String): List<Repo> {
        val repos = withTimeout(HTTP_TIMEOUT.toLong()) {
            httpClient.get<List<GitLabRepo>>("$API_URL/group/$name/allRepos?codeProvider=$GIT_LAB")
        }
        return createRepoModels(repos)
    }

    private fun createRepoModels(repos: List<GitLabRepo>): List<Repo> {
        val result = mutableListOf<Repo>()
        repos.forEach { r ->
            result += Repo(
                id = r.id,
                name = r.name,
                description = r.description ?: "",
                webUrl = r.webUrl,
                totalOpenIssues = r.totalOpenIssues,
                buildSystem = r.buildSystem,
                languages = r.languages
            )
        }
        return result
    }
}
