package org.digieng.repostats.model

import kotlinx.serialization.Serializable
import org.digieng.repostats.BuildSystem

@Serializable
data class GitLabRepo(
    val id: String,
    val name: String,
    val description: String?,
    val webUrl: String,
    val totalOpenIssues: Long,
    val buildSystem: BuildSystem,
    val languages: Map<String, Double>
)
