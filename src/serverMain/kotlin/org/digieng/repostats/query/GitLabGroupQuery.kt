package org.digieng.repostats.query

import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import org.digieng.repostats.BuildSystem
import org.digieng.repostats.HTTP_TIMEOUT
import org.digieng.repostats.model.GitLabRepo
import org.digieng.repostats.model.Repo
import io.ktor.client.request.*
import kotlinx.coroutines.withTimeout
import org.digieng.repostats.httpClient
import org.digieng.repostats.model.GitLabRepoFile

actual object GitLabGroupQuery : GroupQuery {
    private const val API_URL = "https://gitlab.com/api/v4"

    override suspend fun allRepositoriesById(id: String): List<Repo> {
        TODO("not implemented")
    }

    override suspend fun allRepositoriesByName(name: String): List<Repo> {
        val repos = withTimeout(HTTP_TIMEOUT.toLong()) {
            httpClient.get<List<GitLabRepo>>("$API_URL/groups/$name/projects")
        }
        processGitLabRepositories(repos)
        return createRepoModels(repos)
    }

    private fun createRepoModels(repos: List<GitLabRepo>): List<Repo> {
        val result = mutableListOf<Repo>()
        repos.forEach { r ->
            result += Repo(
                id = r.id,
                name = r.name,
                description = r.description ?: "",
                webUrl = r.webUrl,
                totalOpenIssues = r.totalOpenIssues,
                buildSystem = r.buildSystem,
                languages = r.languages
            )
        }
        return result
    }

    private suspend fun processGitLabRepositories(repos: List<GitLabRepo>) = coroutineScope {
        repos.forEach { r ->
            launch {
                val buildSystem = async { buildSystemType(r.id) }
                val languages = async { languagesUsed(r.id) }
                r.buildSystem = buildSystem.await()
                r.languages = languages.await()
            }
        }
    }


    private suspend fun languagesUsed(repoId: String): Map<String, Double> = withTimeout(HTTP_TIMEOUT.toLong()) {
        httpClient.get("$API_URL/projects/$repoId/languages")
    }

    private suspend fun buildSystemType(repoId: String): BuildSystem {
        var result = BuildSystem.UNKNOWN
        for (bs in BuildSystem.values()) {
            if (bs.fileName.isNotEmpty()) {
                val url = "$API_URL/projects/$repoId/repository/files/" +
                    "${bs.fileName.replace(".", "%2E")}?ref=master"
                try {
                    withTimeout(HTTP_TIMEOUT.toLong()) {
                        httpClient.get<GitLabRepoFile>(url)
                    }
                    result = bs
                } catch (ex: Exception) {
                    // Do nothing.
                }
            }
        }
        return result
    }
}
