package org.digieng.repostats.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class GitLabRepoFile(
    val id: String?,
    @SerialName("file_path")
    val filePath: String,
    @SerialName("file_name")
    val fileName: String,
    val size: Long
)
