package org.digieng.repostats.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient
import org.digieng.repostats.BuildSystem

@Serializable
data class GitLabRepo(
    val id: String,
    val name: String,
    val description: String?,
    @SerialName("web_url")
    val webUrl: String,
    @SerialName("open_issues_count")
    val totalOpenIssues: Long
) {
    @Transient
    lateinit var buildSystem: BuildSystem
    @Transient
    lateinit var languages: Map<String, Double>

    override fun toString(): String {
        val buildSystemStr = if (::buildSystem.isInitialized) ", buildSystem=$buildSystem"
        else ", buildSystem=${BuildSystem.UNKNOWN}"
        val languagesStr = if (::languages.isInitialized) ", languages=$languages" else ""
        return "GitLabRepo(id=$id, name=$name, totalOpenIssues=$totalOpenIssues$languagesStr$buildSystemStr"
    }
}
