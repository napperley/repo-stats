package org.digieng.repostats

import io.ktor.application.call
import io.ktor.application.install
import io.ktor.client.*
import io.ktor.client.engine.apache.*
import io.ktor.features.ContentNegotiation
import io.ktor.features.StatusPages
import io.ktor.html.respondHtml
import io.ktor.http.HttpStatusCode
import io.ktor.http.content.resources
import io.ktor.http.content.static
import io.ktor.request.path
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.routing.*
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import kotlinx.html.*
import org.digieng.repostats.query.GitLabGroupQuery

private const val STATIC_DIR = "/static"
internal const val HTTP_TIMEOUT = 5000
private const val GIT_LAB = "git_lab"

internal val httpClient = HttpClient(Apache)

fun main() {
    embeddedServer(factory = Netty, port = 8080) {
        install(StatusPages, setupStatusPages())
        install(ContentNegotiation, setupSerialization())
        routing {
            static(STATIC_DIR, setupStaticRoutes())
            setupGroupRoutes(this)
            baseRoute(this)
            appRoute(this)
        }
    }.start(wait = true)
}

private fun setupSerialization(): ContentNegotiation.Configuration.() -> Unit = {

}

private fun setupStaticRoutes(): Route.() -> Unit = {
    resources("static.js")
}

private fun setupStatusPages(): StatusPages.Configuration.() -> Unit = {
    status(HttpStatusCode.NotFound) { call.respondText("Cannot find ${call.request.path()}") }
    status(HttpStatusCode.InternalServerError) { call.respondText("Server is throwing a wobbly, bugger!") }
}

private fun setupGroupRoutes(routing: Routing) = routing.route("group") {
    allReposByGroupNameRoute(this)
}

private fun appRoute(routing: Routing) = routing.get("/app") {
    val scriptSources = listOf(
        "$STATIC_DIR/kotlin.js",
        "$STATIC_DIR/kotlinx-coroutines-core.js",
        "$STATIC_DIR/webscene-core-0.1.js",
        "$STATIC_DIR/kotlinx-serialization-runtime-js.js",
        "$STATIC_DIR/repo-stats-client.js"
    )
    call.respondHtml {
        head { title("Repo Stats") }
        body {
            div { id = "main-layout" }
            scriptSources.forEach { src -> script(block = createJsScript(src)) }
        }
    }
}

private fun allReposByGroupNameRoute(route: Route) = route.get("{groupName}/allRepos") {
    val codeProvider = call.request.queryParameters["codeProvider"] ?: ""
    val groupName = call.parameters["groupName"] ?: ""
    if (codeProvider == GIT_LAB) {
        println("Fetching repositories for $groupName group on $codeProvider...")
        call.respond(GitLabGroupQuery.allRepositoriesByName(groupName))
    } else {
        call.respond(mapOf("error" to "Code provider is unsupported"))
    }
}

private fun createJsScript(newSrc: String): SCRIPT.() -> Unit = {
    type = "text/javascript"
    src = newSrc
}

private fun baseRoute(routing: Routing) = routing.get("/") {
    call.respondText("Cannot access REST API here.")
}
