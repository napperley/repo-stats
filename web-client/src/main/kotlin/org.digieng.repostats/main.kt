package org.digieng.repostats

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.await
import kotlinx.coroutines.launch
import org.digieng.repostats.model.Repo
import org.w3c.dom.Element
import org.w3c.dom.events.Event
import org.w3c.fetch.Response
import org.webscene.core.HttpMethod
import org.webscene.core.dom.DomEditor.replaceElement
import org.webscene.core.dom.DomQuery.elementById
import org.webscene.core.dom.disabled
import org.webscene.core.dom.focus
import org.webscene.core.dom.hidden
import org.webscene.core.dom.textBoxValue
import org.webscene.core.fetchData
import org.webscene.core.html.element.input.InputType
import kotlinx.serialization.json.JSON.Companion.parse as parseJson
import org.webscene.core.html.HtmlCreator as html
import org.webscene.core.html.HtmlInputCreator as htmlInput

private val labels = mapOf(
    "main-lang" to "Main Language: ",
    "avg-open-issues" to "Average Open Issues: ",
    "langs-used" to "Languages Used: ",
    "most-used-lang" to "Most Used Language: ",
    "main-build-sys" to "Main Build System: "
)

private fun createStatsLayout() = html.div {
    id = "stats-layout"
    hidden = true
    children += html.lineBreak {}
    labels.keys.forEach { k ->
        children += html.bold { +"${labels[k]}" }
        children += html.span { id = k }
        children += html.lineBreak {}
    }
}

private fun createMainLayout() = html.div {
    id = "main-layout"
    children += html.heading { +"Repo Stats" }
    // TODO: Include combo box for code provider (Git Lab, Git Hub etc).
    children += html.span { +"Group: " }
    children += htmlInput.input(type = InputType.TEXT, autoFocus = true) { id = "group-txt" }
    children += htmlInput.button {
        +"Show Statistics"
        id = "stats-btn"
    }
    children += createLoadingMsg()
    children += createStatsLayout()
}

private fun createLoadingMsg(group: String = "", hide: Boolean = true) = html.paragraph {
    id = "loading-msg"
    hidden = hide
    +"Loading repository statistics for $group group..."
}

fun main() {
    replaceElement { createMainLayout() }
    elementById<Element>("stats-btn").addEventListener("click", ::onStatsBtnClick)
}

@Suppress("UNUSED_PARAMETER")
private fun onStatsBtnClick(evt: Event) {
    val group = elementById<Element>("group-txt").textBoxValue
    val url = "http://localhost:8080/group/$group/allRepos?codeProvider=git_lab"
    elementById<Element>("stats-btn").disabled(true)
    elementById<Element>("stats-layout").hidden(true)
    replaceElement { createLoadingMsg(group, false) }
    fetchData(
        url = url,
        method = HttpMethod.GET,
        onError = ::onFetchReposError,
        onResponse = ::processFetchedRepos
    )
}

private fun processFetchedRepos(resp: Response) = GlobalScope.launch {
    if (resp.ok) {
        val repos: Array<Repo> = JSON.parse(resp.text().await())
        // TODO: Remove the line below.
        repos.forEach { println("* ${it.name} - ${it.description}") }
        updateStatsLayout(repos)
    }
    replaceElement { createLoadingMsg() }
    elementById<Element>("group-txt").focus()
    elementById<Element>("stats-btn").disabled(false)
}

private fun updateStatsLayout(repos: Array<Repo>) {
    val mainLang = GroupStatistics.mainLanguage(repos)
    val avgOpenIssues = GroupStatistics.averageOpenIssues(repos)
    val langsUsed = GroupStatistics.languagesUsed(repos)
    val mostUsedLang = GroupStatistics.mostUsedLanguage(repos)
    val mainBuildSys = GroupStatistics.mainBuildSystem(repos)

    elementById<Element>("main-lang").textContent = mainLang
    elementById<Element>("avg-open-issues").textContent = "$avgOpenIssues"
    elementById<Element>("langs-used").textContent = "$langsUsed"
    elementById<Element>("most-used-lang").textContent = mostUsedLang
    elementById<Element>("main-build-sys").textContent =
        if (mainBuildSys != BuildSystem.UNKNOWN) mainBuildSys.systemName else "Unknown"
    elementById<Element>("stats-layout").hidden(false)
}

private fun onFetchReposError(err: Throwable) {
    println("Can't fetch repositories: ${err.message}")
    replaceElement { createLoadingMsg() }
    elementById<Element>("group-txt").focus()
    elementById<Element>("stats-btn").disabled(false)
}
