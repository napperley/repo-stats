import org.jetbrains.kotlin.gradle.tasks.Kotlin2JsCompile

val moduleName = "$rootProjectName-${project.name}"

repositories {
    jcenter()
    mavenCentral()
    maven { url = uri("libs") }
    maven { url = uri("https://kotlin.bintray.com/kotlinx") }
}

buildscript {
    repositories {
        mavenCentral()
        jcenter()
    }

    dependencies {
        classpath(kotlin(module = "gradle-plugin", version = Version.kotlin))
        classpath(Dependency.kotlinxSerialization)
    }
}

apply {
    plugin(PluginId.kotlinJsPlatform)
    plugin(PluginId.kotlin2Js)
    plugin(PluginId.kotlinxSerialization)
}

dependencies {
    "expectedBy"(project(":common"))
    "compile"(kotlin(module = "stdlib-js", version = Version.kotlin))
    "compile"(Dependency.websceneCoreJs)
    "compile"(Dependency.kotlinxSerializationRuntimeJs)
    "compile"(Dependency.coroutinesCoreJs)
}

val webDir = "${projectDir.absolutePath}/web"
val compileKotlin2Js by tasks.getting(Kotlin2JsCompile::class) {
    val fileName = "repo-stats-client.js"

    kotlinOptions.outputFile = "$webDir/js/$fileName"
    kotlinOptions.sourceMap = true
    doFirst { File("$webDir/js").deleteRecursively() }
}
val build by tasks
val assembleWeb by tasks.creating(Copy::class) {
    dependsOn("classes")
    configurations["compile"].forEach { file ->
        from(zipTree(file.absolutePath)) {
            includeEmptyDirs = false
            include { fileTreeElement ->
                val path = fileTreeElement.path
                path.endsWith(".js") && path.startsWith("META-INF/resources/") ||
                    !path.startsWith("META_INF/")
            }
        }
    }
    from(compileKotlin2Js.destinationDir)
    into("$webDir/js")
}

task<Sync>("deployClientToServer") {
    val destDir = "${projectDir.parent}/server/src/main/resources/static"
    dependsOn(compileKotlin2Js, assembleWeb)
    from(webDir)
    into(destDir)
}
