group = "org.digieng"
version = "0.1-SNAPSHOT"

plugins {
    kotlin("multiplatform") version Version.kotlin
    kotlin("plugin.serialization") version Version.kotlin
    application
}

application {
    mainClassName = "org.digieng.repostats.MainKt"
}

repositories {
    mavenCentral()
    jcenter()
}

kotlin {
    jvm("server") {
        dependencies {
            implementation(Dependency.serializationJson)
            implementation(Dependency.ktorNetty)
            implementation(Dependency.ktorHtmlBuilder)
            implementation(Dependency.ktorClientCore)
            implementation(Dependency.ktorClientApache)
            implementation(Dependency.log4j)
        }
        withJava()
    }

    jvm("cliClient") {
        dependencies {
            // TODO: Find out why dependencies aren't being applied to the cliClient module.
            implementation(Dependency.serializationJson)
            implementation(Dependency.coroutinesCore)
            implementation(Dependency.ktorClientCore)
            implementation(Dependency.ktorClientApache)
        }
    }

    sourceSets {
        commonMain {
            dependencies {
                implementation(Dependency.serializationJson)
            }
        }
    }
}

tasks.getByName("run") {
    doFirst { println("Starting server...") }
    doLast { println("Server ready.") }
}
