const val rootProjectName = "repo-stats"

object Version {
    const val kotlin = "1.4.10"
    const val ktor = "1.4.2"
    const val log4j = "1.7.25"
    const val coroutines = "1.4.1"
    const val serialization = "1.0.1"
}

object Dependency {
    const val serializationJson = "org.jetbrains.kotlinx:kotlinx-serialization-json:${Version.serialization}"
    const val ktorNetty = "io.ktor:ktor-server-netty:${Version.ktor}"
    const val log4j = "org.slf4j:slf4j-log4j12:${Version.log4j}"
    const val coroutinesCore = "org.jetbrains.kotlinx:kotlinx-coroutines-core:${Version.coroutines}"
    const val ktorHtmlBuilder = "io.ktor:ktor-html-builder:${Version.ktor}"
    const val ktorClientCore = "io.ktor:ktor-client-core:${Version.ktor}"
    const val ktorClientApache = "io.ktor:ktor-client-apache:${Version.ktor}"
}
